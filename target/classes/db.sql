
CREATE TABLE IF NOT EXISTS parent (
  parent_id INT NOT NULL,
  email VARCHAR(45) NOT NULL,
  fname VARCHAR(45) NOT NULL,
  lname VARCHAR(45) NOT NULL,
  dob VARCHAR(45) NOT NULL,
  phone VARCHAR(45) NOT NULL,
  mobile VARCHAR(45) NOT NULL,
  status SMALLINT NOT NULL,
  last_login_date DATE NOT NULL,
  last_login_id DATE NOT NULL,
  PRIMARY KEY (parent_id))
;


-- -----------------------------------------------------
-- Table `mydb`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS  student (
  student_id INT NOT NULL,
  email VARCHAR(45) NOT NULL,
  password VARCHAR(45) NOT NULL,
  lname VARCHAR(45) NOT NULL,
  dob VARCHAR(45) NOT NULL,
  phone VARCHAR(45) NOT NULL,
  mobile VARCHAR(45) NOT NULL,
  studentcol INT NOT NULL,
  date_of_join DATE NOT NULL,
  status SMALLINT NOT NULL,
  last_login_date DATE NOT NULL,
  last_login_id VARCHAR(45) NOT NULL,
  parent_id INT NOT NULL,
  PRIMARY KEY (student_id)
 ,
  CONSTRAINT fk_student_parent
    FOREIGN KEY (parent_id)
    REFERENCES parent (parent_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_student_parent_idx ON student (parent_id ASC);


-- -----------------------------------------------------
-- Table `mydb`.`teacher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS teacher (
  teacher_id INT NOT NULL,
  email VARCHAR(45) NULL,
  password VARCHAR(45) NULL,
  fname VARCHAR(45) NULL,
  lname VARCHAR(45) NULL,
  dob DATE NULL,
  phone VARCHAR(45) NULL,
  mobile VARCHAR(45) NULL,
  status SMALLINT NULL,
  last_login_date DATE NULL,
  last_login_id VARCHAR(45) NULL,
  PRIMARY KEY (teacher_id))
;


-- -----------------------------------------------------
-- Table `mydb`.`grade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS grade (
  grade_id INT NOT NULL,
  name VARCHAR(45) NOT NULL,
  descrip VARCHAR(45) NOT NULL,
  PRIMARY KEY (grade_id))
;


-- -----------------------------------------------------
-- Table `mydb`.`course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS course (
  course_id INT NOT NULL,
  name VARCHAR(45) NOT NULL,
  description VARCHAR(45) NOT NULL,
  grade_id INT NOT NULL,
  PRIMARY KEY (course_id)
 ,
  CONSTRAINT fk_course_grade1
    FOREIGN KEY (grade_id)
    REFERENCES grade (grade_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_course_grade1_idx ON course (grade_id ASC);


-- -----------------------------------------------------
-- Table `mydb`.`exam_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS exam_type (
  exam_type_id INT NOT NULL,
  name VARCHAR(45) NOT NULL,
  descrip VARCHAR(45) NOT NULL,
  PRIMARY KEY (exam_type_id))
;


-- -----------------------------------------------------
-- Table `mydb`.`exam`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS exam (
  exam_id INT NOT NULL,
  name VARCHAR(45) NULL,
  start_date DATE NULL,
  exam_type_id INT NOT NULL,
  PRIMARY KEY (exam_id)
 ,
  CONSTRAINT fk_exam_exam_type1
    FOREIGN KEY (exam_type_id)
    REFERENCES exam_type (exam_type_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_exam_exam_type1_idx ON exam (exam_type_id ASC);


-- -----------------------------------------------------
-- Table `mydb`.`exam_result`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS exam_result (
  marks VARCHAR(45) NOT NULL,
  student_id INT NOT NULL,
  course_id INT NOT NULL,
  exam_id INT NOT NULL
 ,
  CONSTRAINT fk_exam_result_student1
    FOREIGN KEY (student_id)
    REFERENCES student (student_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_exam_result_course1
    FOREIGN KEY (course_id)
    REFERENCES course (course_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_exam_result_exam1
    FOREIGN KEY (exam_id)
    REFERENCES exam (exam_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_exam_result_student1_idx ON exam_result (student_id ASC);
CREATE INDEX fk_exam_result_course1_idx ON exam_result (course_id ASC);
CREATE INDEX fk_exam_result_exam1_idx ON exam_result (exam_id ASC);


-- -----------------------------------------------------
-- Table `mydb`.`classroom`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS classroom (
  classroom_id INT NOT NULL,
  year VARCHAR(45) NOT NULL,
  section CHAR(2) NOT NULL,
  status SMALLINT NOT NULL,
  remarks VARCHAR(45) NOT NULL,
  grade_id INT NOT NULL,
  teacher_id INT NOT NULL,
  PRIMARY KEY (classroom_id)
 ,
  CONSTRAINT fk_classroom_grade1
    FOREIGN KEY (grade_id)
    REFERENCES grade (grade_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_classroom_teacher1
    FOREIGN KEY (teacher_id)
    REFERENCES teacher (teacher_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_classroom_grade1_idx ON classroom (grade_id ASC);
CREATE INDEX fk_classroom_teacher1_idx ON classroom (teacher_id ASC);


-- -----------------------------------------------------
-- Table `mydb`.`classroom_student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS classroom_student (
  classroom_id INT NOT NULL,
  student_id INT NOT NULL
 ,
  CONSTRAINT fk_classroom_student_classroom1
    FOREIGN KEY (classroom_id)
    REFERENCES classroom (classroom_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_classroom_student_student1
    FOREIGN KEY (student_id)
    REFERENCES student (student_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_classroom_student_classroom1_idx ON classroom_student (classroom_id ASC);
CREATE INDEX fk_classroom_student_student1_idx ON classroom_student (student_id ASC);


-- -----------------------------------------------------
-- Table `mydb`.`attendance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS attendance (
  date DATE NOT NULL,
  status SMALLINT NOT NULL,
  remark TEXT NOT NULL,
  student_id INT NOT NULL,
  PRIMARY KEY (date)
 ,
  CONSTRAINT fk_attendance_student1
    FOREIGN KEY (student_id)
    REFERENCES student (student_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_attendance_student1_idx ON attendance (student_id ASC);


/* SET SQL_MODE=@OLD_SQL_MODE; */
/* SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS; */
/* SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS; */