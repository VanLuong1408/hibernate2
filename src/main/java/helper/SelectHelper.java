package helper;

import model.FatherModel;
import model.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vanluong on 27/05/2017.
 */
public class SelectHelper {

    private SessionFactory factory;

    public SelectHelper(SessionFactory factory){
        this.factory=factory;
    }

    public List<Student> getAllStudent(){
        Session session=factory.openSession();
        Transaction transaction= session.beginTransaction();
        List<Student> studentList=new ArrayList<Student>();

        try {
            studentList= (List<Student>) session.createQuery("FROM Student").list();
            transaction.commit();
        }catch (HeadlessException e){
            e.printStackTrace();
        }

        finally {
            session.close();
        }

        return studentList;
    }

    public List<Student> getStudentByID(int id){
        Session session=factory.openSession();
        Transaction transaction= session.beginTransaction();
        List<Student> studentList=new ArrayList<Student>();

        studentList= (List<Student>) session.createQuery("FROM Student s WHERE s.studentId = "+ id).list();
        transaction.commit();

        session.close();

        return studentList;
    }
}
