package helper;

import model.Student;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * Created by vanluong on 27/05/2017.
 */
public class UpdateHelper {

    private SessionFactory factory;

    public UpdateHelper(SessionFactory factory){
        this.factory=factory;
    }

    public void updateEmailStudent(String email, int id) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Student student= (Student) session.get(Student.class, id);
        student.setEmail(email);

        session.update(student);
        transaction.commit();
        session.close();

        System.out.println("update done");

    }
}
