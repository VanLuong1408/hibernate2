package helper;

import model.Student;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * Created by vanluong on 27/05/2017.
 */
public class DeleteHelper {

    private SessionFactory factory;

    public DeleteHelper(SessionFactory factory){
        this.factory=factory;
    }

    public void deleteColoumStudent(int id){
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Student student= (Student) session.get(Student.class, id);

        session.delete(student);
        transaction.commit();
        session.close();
    }
}
