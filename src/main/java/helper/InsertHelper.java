package helper;

import model.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * Created by vanluong on 27/05/2017.
 */
public class InsertHelper {

    private SessionFactory factory;

    public InsertHelper(SessionFactory factory){
        this.factory=factory;
    }

    private void saveDB(FatherModel fatherModel){
        Session session=factory.openSession();
        Transaction transaction= session.beginTransaction();

        try {
            session.save(fatherModel);
            transaction.commit();

        }catch (HibernateException er){
            if (transaction!=null){
                transaction.rollback();
            }

            er.printStackTrace();
        }

        finally {
            session.close();

        }
    }

    public void insertTeacher(Teacher teacher){
        saveDB(teacher);
    }

    public void insertAttendance(Attendance attendance){
        saveDB(attendance);

    }

    public void insertParent(Parent parent){
        saveDB(parent);
    }

    public void insertClassRoom(ClassRoom classRoom){
        saveDB(classRoom);
    }

    public void insertClassRoomStudent(ClassRoomStudent classRoomStudent){
        saveDB(classRoomStudent);
    }

    public void insertCourse(Course course){
        saveDB(course);
    }

    public void insertExam(Exam exam){
        saveDB(exam);
    }

    public void insertExamResult(ExamResult examResult){
        saveDB(examResult);
    }
    public void insertExamType(ExamType examType){
        saveDB(examType);
    }

    public void insertStudent(Student student){
        saveDB(student);
    }

    public void insertGrade(Grade grade){
        saveDB(grade);
    }

}
