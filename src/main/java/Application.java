import helper.DeleteHelper;
import helper.InsertHelper;
import helper.SelectHelper;
import helper.UpdateHelper;
import model.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.Date;
import java.util.List;

/**
 * Created by vanluong on 27/05/2017.
 */
public class Application {

    private static SessionFactory sessionFactory;
    private static InsertHelper helper;
    private static SelectHelper selectHelper;
    private static UpdateHelper updateHelper;
    private static DeleteHelper deleteHelper;

    public static void main(String[] args) {
        // loads configuration and mappings
        Configuration configuration = new Configuration().configure();
        ServiceRegistryBuilder registry = new ServiceRegistryBuilder();
        registry.applySettings(configuration.getProperties());
        ServiceRegistry serviceRegistry = registry.buildServiceRegistry();

        // builds a session factory from the service registry
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);


        helper = new InsertHelper(sessionFactory);
        selectHelper = new SelectHelper(sessionFactory);
        updateHelper = new UpdateHelper(sessionFactory);
        deleteHelper = new DeleteHelper(sessionFactory);

        insert();

        display();
        updateHelper.updateEmailStudent("phamvanluongk59@gmail.com", 0);

        //deleteHelper.deleteColoumStudent(0);


    }

    private static void display() {
        List<Student> studentList = selectHelper.getAllStudent();

        for (int i = 0; i < studentList.size(); i++) {
            System.out.println(studentList.get(i).getFname() + " " + studentList.get(i).getLname());
        }

        List<Student> studentList1 = selectHelper.getStudentByID(0);

        for (int i = 0; i < studentList1.size(); i++) {
            System.out.println(studentList1.get(i).getFname() + " " + studentList1.get(i).getLname());
        }


    }

    private static void insert() {
        Teacher teacher = new Teacher();
        teacher.setTeacher_id(0);
        teacher.setEmail("fdf");
        teacher.setPassword("fdfd");
        teacher.setFname("fsd");
        teacher.setLname("dfdf");
        Date date = new Date();
        teacher.setDob(date);
        teacher.setPhone("dsdsd");
        teacher.setMobile("dfdf");
        teacher.setStatus(true);
        teacher.setLastLoginDate(date);
        teacher.setLastLoginID("dsdsd");

        Parent parent = new Parent();
        parent.setParent_id(0);
        parent.setEmail("df");
        parent.setFname("fds");
        parent.setLname("dfdfdf");
        parent.setDob(date);
        parent.setPhone("dfdfdf");
        parent.setMobile("dfdfd");
        parent.setLastLoginDate(date);
        parent.setLastLoginID("dfdfdf");

        Grade grade = new Grade();
        grade.setGrade_id(0);
        grade.setDescrip("Fdfdf");
        grade.setName("Ddds");

        Student student = new Student();
        student.setStudentId(0);
        student.setEmail("Dsdsd");
        student.setPassword("dfdf");
        student.setLname("fdfdf");
        student.setFname("Dfdf");
        student.setDob(date);
        student.setStudentcol(2);
        student.setDateOfJoin(date);
        student.setPhone("dsdsd");
        student.setMobile("sdsdsd");
        student.setStatus(true);
        student.setLastLoginDate(date);
        student.setLastLoginID("dsds");
        student.setParent(parent);

        ClassRoom classRoom = new ClassRoom();
        classRoom.setClassRoomId(0);
        classRoom.setYear("323");
        classRoom.setSection("ds");
        classRoom.setRemarks("Dsdsd");
        classRoom.setGrade(grade);
        classRoom.setTeacher(teacher);
        classRoom.setStatus(false);

        helper.insertTeacher(teacher);
        helper.insertParent(parent);
        helper.insertGrade(grade);
        helper.insertStudent(student);
        helper.insertClassRoom(classRoom);

        Teacher teacher1 = new Teacher();
        teacher.setTeacher_id(3);
        teacher.setEmail("fdf");
        teacher.setPassword("fdfd");
        teacher.setFname("fsd");
        teacher.setLname("dfdf");
        teacher.setDob(date);
        teacher.setPhone("dsdsd");
        teacher.setMobile("dfdf");
        teacher.setStatus(true);
        teacher.setLastLoginDate(date);
        teacher.setLastLoginID("dsdsd");

        Parent parent1 = new Parent();
        parent.setParent_id(3);
        parent.setEmail("df");
        parent.setFname("fds");
        parent.setLname("dfdfdf");
        parent.setDob(date);
        parent.setPhone("dfdfdf");
        parent.setMobile("dfdfd");
        parent.setLastLoginDate(date);
        parent.setLastLoginID("dfdfdf");

        Grade grade1 = new Grade();
        grade.setGrade_id(3);
        grade.setDescrip("Fdfdf");
        grade.setName("Ddds");

        Student student1 = new Student();
        student.setStudentId(3);
        student.setEmail("Dsdsd");
        student.setPassword("dfdf");
        student.setLname("fdfdf");
        student.setFname("Dfdf");
        student.setDob(date);
        student.setStudentcol(2);
        student.setDateOfJoin(date);
        student.setPhone("dsdsd");
        student.setMobile("sdsdsd");
        student.setStatus(true);
        student.setLastLoginDate(date);
        student.setLastLoginID("dsds");
        student.setParent(parent1);

        ClassRoom classRoom1 = new ClassRoom();
        classRoom.setClassRoomId(3);
        classRoom.setYear("35s55");
        classRoom.setSection("ds");
        classRoom.setRemarks("Dsdsd");
        classRoom.setGrade(grade1);
        classRoom.setTeacher(teacher1);
        classRoom.setStatus(false);

        helper.insertTeacher(teacher1);
        helper.insertParent(parent1);
        helper.insertGrade(grade1);
        helper.insertStudent(student1);
        helper.insertClassRoom(classRoom1);


    }
}
