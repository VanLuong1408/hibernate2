package model;

import javax.persistence.*;

/**
 * Created by vanluong on 26/05/2017.
 */
@Entity
@Table(name = "classroom_student")
public class ClassRoomStudent extends FatherModel {

    @Id
    @Column(name = "classroom_student")
    private int classRoomStudentId;

    @ManyToOne
    @JoinColumn(name = "classroom_id")
    private ClassRoom classRoom;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    public int getClassRoomStudentId() {
        return classRoomStudentId;
    }

    public void setClassRoomStudentId(int classRoomStudentId) {
        this.classRoomStudentId = classRoomStudentId;
    }

    public ClassRoom getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(ClassRoom classRoom) {
        this.classRoom = classRoom;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
